# Student Database #
Requirements to run:

* IDE that compiles Java code

* Java JRE System Library [JavaSE-1.8]

## About ##
.Java file
GUI made to create a customizable database and save the information to a text file. Can also load the information from the file as well.
Columns and Records (rows)

* original columns are meant to hold information on student-created characters
* Columns can be deleted(including the original columns) and new ones created
* Rows beneath the deleted column and the information they contain are also deleted
* When a new column is created the blank entry is applied to all rows.
* double String array holds all input, including column titles.

**Note: ** If you don't save the information it *will* be lost.

Project was homework 13 for Computer Science 1 class in December of 2015.

Original Repository Name - csci141-fall15-hw13