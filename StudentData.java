import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FileDialog;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

public class StudentData {
	private static final Insets insets = new Insets( 0, 0, 1, 1);
	private static Scanner niu = new Scanner(System.in);
	public static void StudentData(){
		JFrame sb = new JFrame("Student Database");
		sb.setSize(400,250);
		
		sb.setLayout(new BorderLayout());
		Box buttns = new Box(BoxLayout.Y_AXIS);
		JMenuBar barra = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenu rec = new JMenu("Record");
		
		
		String[][] data = { {"", "", ""} };
		String[] colNames = {"Character", "Type", "Kill Count"};
		DefaultTableModel model = new DefaultTableModel(data, colNames);
		JTable info = new JTable(model);
		sb.add(new JScrollPane(info), BorderLayout.CENTER);
		
		JButton record;
		JMenuItem forfl;
		
		forfl = new JMenuItem("New Record");
		forfl.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	String[] vals = {"", "", ""};
		    	model.addRow(vals);
		    }
		});
		rec.add(forfl);
		
		forfl = new JMenuItem("Delete Record");
		forfl.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	int row = info.getSelectedRow();
		    	if(row != -1){ model.removeRow(row);}
		    }
		});
		rec.add(forfl);
		
		forfl = new JMenuItem("Load Records");
		forfl.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		    	FileDialog mostrar = new FileDialog(sb, "Open File", FileDialog.LOAD);
		    	mostrar.setVisible(true);
		    	if(mostrar.getFiles().length>0){
		    		System.out.println(mostrar.getFiles()[0].getAbsolutePath());
		    	}
		    	try{
		    		FileInputStream in = new FileInputStream(mostrar.getFiles()[0].getAbsolutePath());
		    		Scanner get = new Scanner(in);
		    		while(get.hasNext()){
		    			String g = get.nextLine();
			    		String h = get.nextLine();
			    		String i = get.nextLine();
			    		String[] vals = { g, h, i};
			    		model.addRow(vals);
		    		}
		    	} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (SecurityException e2){
					System.out.println("doesn't allow file to be read");
				} catch(IOException e3){
					System.out.println("There is a problem reading the file");
				}
		    }
		});
		file.add(forfl);
		
		forfl = new JMenuItem("Save Records");
		forfl.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        FileDialog salvar = new FileDialog(sb, "Save File", FileDialog.SAVE);
		        salvar.setVisible(true);
		        if(salvar.getFile().length() > 0){
		        	System.out.println(salvar.getFiles()[0].getAbsolutePath());
		        }
		        try {
					PrintWriter lala = new PrintWriter(salvar.getFiles()[0].getAbsolutePath() + ".txt");
					for(int y = 0; y < model.getRowCount(); y++){
						for(int x = 0; x < model.getColumnCount(); x++){
							lala.println(model.getValueAt(y, x));
						}
					}
					lala.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (SecurityException e2){
					System.out.println("doesn't allow file to be read");
				}
		    }
		});
		file.add(forfl);
		
		forfl = new JMenuItem("Quit");
		forfl.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});
		file.add(forfl);
		
		barra.add(file);
		barra.add(rec);
		sb.add(barra, BorderLayout.NORTH);
		
		record = new JButton("Add Column");
		record.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String coln = newColn();
				if(coln != null){
					model.addColumn(coln);
				}
			}
		});
		buttns.add(record);
		
		record = new JButton("Delete Column");
		record.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setColumnCount(model.getColumnCount() - 1);
			}
		});
		buttns.add(record);
		
		info.setRowSorter(new TableRowSorter(model));
		
		sb.add(buttns, BorderLayout.EAST);
						
		sb.pack();
		sb.setVisible(true);
	}
	private static void addComponent(Container container, Component component, int gridx, int gridy, int gridwidth, int gridheight, int anchor, int fill) {
		GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0, 1.0, anchor, fill, insets, 0, 0);
		container.add(component, gbc);
	}
	private static String newColn(){
		JOptionPane ask = new JOptionPane();
		String newcol = ask.showInputDialog("What is the name of the new column?");
		return newcol;	
	}
	public static void main(String[] args) {
		 SwingUtilities.invokeLater(new Runnable() {
		        public void run() {
		            StudentData();
		        }
		    });
	}

}
